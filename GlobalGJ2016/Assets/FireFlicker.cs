﻿using UnityEngine;
using System.Collections;

public class FireFlicker : MonoBehaviour
{
	private Light light;
	private float startTime;
	private float EndTime;
	public float flickeringUpdateSeconds = 0.05f;
	public float lightMinIntensity = 2;
	public float lightMaxIntensity = 8;

    [Range(0.5f,2f)]
    public float step = 0.4f;

	// Use this for initialization
	void Start ()
	{
		light = GetComponent<Light> ();
		startTime = Time.time;
	}

	private void flickerLight ()
	{
		EndTime = Time.time;
		float timePassed = EndTime - startTime;
		if (timePassed > flickeringUpdateSeconds) {

            float tfloat = Random.Range(light.intensity - step, light.intensity + step);

            if(tfloat > lightMaxIntensity)
            {
                tfloat = lightMaxIntensity;
            }
            else if(tfloat < lightMinIntensity)
            {
                tfloat = lightMinIntensity;
            }

            light.intensity = tfloat;
			startTime = Time.time;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		flickerLight ();
	}
}
