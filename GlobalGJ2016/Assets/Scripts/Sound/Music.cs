﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

public class Music : MonoBehaviour
{
    public List<float> Clips = new List<float>();

    public AudioMixer Mixer = null;
    public AudioMixerSnapshot StemA = null;
    public AudioMixerSnapshot StemB = null;

    void Update()
    {
        for (int i=0; i< Clips.Count; i++)
        {
            if (GameTime.TIME < Clips[i])
            {
                Clips.Remove(Clips[i]);
                StemB.TransitionTo(1f);
                //Mixer.TransitionToSnapshots(SS, null, 1f);
            }
        }
    }
}