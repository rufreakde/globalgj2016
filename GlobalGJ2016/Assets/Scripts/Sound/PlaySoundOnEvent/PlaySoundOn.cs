﻿/*********************
*	Rudolf Chrispens
***********************/

#region USE
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Audio;
#endregion

namespace Dev6
{
    //[SelectionBase]
    [AddComponentMenu("Audio/Play Sound On Event")]
    public class PlaySoundOn : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        //[Split("PlaySoundOnMouse")]
        public bool DIRECT_SOUND = true;

        [Header("#State:")]
        public PlaySoundSettings _Awake = new PlaySoundSettings();
        public PlaySoundSettings _Start = new PlaySoundSettings();
        public PlaySoundSettings _OnEnable = new PlaySoundSettings();
        public PlaySoundSettings _OnDisable = new PlaySoundSettings();


        [Header("#Mouse/Touch:")]
        public PlaySoundSettings _OnClick = new PlaySoundSettings();
        public PlaySoundSettings _OnEnter = new PlaySoundSettings();
        public PlaySoundSettings _OnExit = new PlaySoundSettings();

        private AudioSource DirectSource = null;

        #region State play
        void Awake()
        {
            //always adds a source to play a sound
            DirectSource = this.gameObject.AddComponent<AudioSource>();

            if (_Awake.Play)
                PlaySound(_Awake);
        }

        void Start()
        {
            if (_Start.Play)
                PlaySound(_Start);
        }

        void OnEnable()
        {
            if (_OnEnable.Play)
                PlaySound(_OnEnable);
        }

        void OnDisable()
        {
            if (_OnDisable.Play)
                PlaySound(_OnDisable);
        }
        #endregion

        #region Mouse/Touch/Eventsystem play

        public void OnMouseDown()
        {
            Debug.Log("Mouse Click");
            if (_OnClick.Play)
                PlaySound(_OnClick);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_OnClick.Play)
                PlaySound(_OnClick);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (_OnEnter.Play)
                PlaySound(_OnEnter);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_OnEnter.Play)
                PlaySound(_OnEnter);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_OnExit.Play)
                PlaySound(_OnExit);
        }
        #endregion

        void PlaySound(PlaySoundSettings _Set)
        {
            if (_Set.Clip)
            {
                if (DIRECT_SOUND)
                {
                    DirectSource.PlayOneShot(_Set.Clip);
                }
                else if( _Set.Mixer != null)
                {
                    DirectSource.outputAudioMixerGroup = _Set.Mixer;
                    DirectSource.PlayOneShot(_Set.Clip);
                }
                else
                {
                    Debug.LogError("Error: (" + this.ToString() + ")  Clip: "+ _Set.Clip.name +" is set on Directsound = (" + DIRECT_SOUND + ") but there is no mixer chosen!" );
                }
            }
        }



        [System.Serializable]
        public class PlaySoundSettings
        {

            public bool Play = false;
            public AudioClip Clip = null;
            public AudioMixerGroup Mixer = null;

            public PlaySoundSettings()
            {
                Play = false;
                Clip = null;
                Mixer = null;
            }
        }
    }

}