﻿/*********************
*	Rudolf Chrispens
***********************/

#region USE
using UnityEngine;
using System.Collections;
using Dev6;
#endregion

namespace Dev6
{
//[SelectionBase]
public class SpawnObj2D : MonoBehaviour 
{
    #region variables
    //[Split("SpawnObj2D")]
    public bool _OnDisable = true;
    public bool _OnDestroy = false;
    public bool _OnAwake = false;
    public bool _OnStart = false;
    public float _OnTimed = 0f;
    private float _onTimed = 0f;

        [Space(20f)]
    public bool RandomlyOutOfArray = false;
    //[HideIfNot("RandomlyOutOfArray", true)]
    [Tooltip("This will be enabled if RandomlyOutOfArray is active")]
    public int RandomCount = 1;

    public CSpawnableObject2D[] Container = new CSpawnableObject2D[1];

    private bool GameIsPlaying = true;	//true as long the game is running false if the game is closed so no spawn when false
    private GameObject tGameObject; //alloc memory

    void OnApplicationQuit()
    {
        GameIsPlaying = false;
    }

        /// <summary>
        /// Here Philip use this to spawn the defined prefab over your handler script! It will spawn at the defined definition of the SpawnObj2D script.
        /// </summary>
        public void Spawn()
    {
        if (GameIsPlaying)
            StartSpawning();
    }

    void Awake()
    {
        EnableAllDisableAll(CheckForNonGameObjects()); //no game object error handling
        CheckValidSettings();

            _onTimed = _OnTimed;

        if (_OnAwake && GameIsPlaying)
            StartSpawning();
    }

    void OnDisable()
    {
        if (_OnDisable && GameIsPlaying)
            StartSpawning();
    }

    void OnDestroy()
    {
        if (_OnDestroy && GameIsPlaying)
            StartSpawning();
    }

    void Start()
    {
        if (_OnStart && GameIsPlaying)
            StartSpawning();
    }

    void Update()
    {
        _OnTimed -= Time.deltaTime;

        if (_OnTimed < 0f && GameIsPlaying)
        {
            StartSpawning();
            _OnTimed = _onTimed;
        }
    }

        #endregion

        private void CheckValidSettings()
        {
            for (int i = 0; i < Container.Length; i++)
            {
                if (Container[i].RandomXmax < Container[i].RandomXmin)
                    Debug.LogError("RandomXmax is smaller than RandomXmin! : " + this.ToString());
                if (Container[i].RandomYmax < Container[i].RandomYmin)
                    Debug.LogError("RandomXmax is smaller than RandomXmin! : " + this.ToString());
                if (Container[i].RandomZmax < Container[i].RandomZmin)
                    Debug.LogError("RandomXmax is smaller than RandomXmin! : " + this.ToString());
            }
        }

    private void StartSpawning()
    {
        //how many? check settings and make spawn calls
        //random out of Array?
        //Random Count?
        if (RandomlyOutOfArray)
        {
            for (int i = 0; i < RandomCount; i++)
            {
                int tRand = Random.Range(0, Container.Length); //choose target out of array
                    SpawnObject(Container[tRand]);
            }
        }
        else
        {
            for (int i = 0; i < Container.Length; i++)
            {
                SpawnObject(Container[i]);
            }
        }
    }

    //Eigentlicher Spawn
    void SpawnObject(CSpawnableObject2D _Object)
    {
        //SetPos
        Vector3 _Position = SetPosition(_Object);
        //SetRot
        Vector3 _Rotation = SetRotation(_Object);
        //SetScale
        float _Scale = SetScale(_Object);

        tGameObject = SpawnAfterPropertiesAreSet(_Object, _Position, Quaternion.Euler(_Rotation), _Scale);
        SetOtherProperties(_Object, tGameObject);
        ApplyPhysicsToGO(_Object, tGameObject);
    }

    Vector3 SetPosition(CSpawnableObject2D _Object)
    {
            if (_Object.RandomSpawn)
            {
                float RandomX = Random.Range(_Object.RandomXmin, _Object.RandomXmax);
                float RandomY = Random.Range(_Object.RandomYmin, _Object.RandomYmax);
                float RandomZ = Random.Range(_Object.RandomZmin, _Object.RandomZmax);

                if (_Object.PreserveParentZPosition)
                {
                    RandomZ = this.transform.position.z;
                }

                return new Vector3(RandomX, RandomY, RandomZ);
            }
            else
            {
                float tZDepth = _Object.RelativeSpawnPos.z;
                if (_Object.PreserveParentZPosition)
                {
                    tZDepth = this.transform.position.z;
                }
                return new Vector3( _Object.RelativeSpawnPos.x, _Object.RelativeSpawnPos.y, tZDepth);
            }
    }

    Vector3 SetRotation(CSpawnableObject2D _Object)
    {
        float tRotation = 0f;

        if (_Object.RotationRandom)
        {
                tRotation = Random.Range(_Object.RotationMin, _Object.RotationMax);
        }
        else
        {
                tRotation = _Object.RotationDegree;
        }

        //Debug.Log("Rotation 2D " + _RotationAngle);
        return new Vector3(0f, 0f, tRotation);
    }

        //Spawn with set properties:
        GameObject SpawnAfterPropertiesAreSet(CSpawnableObject2D _SpawnableObject, Vector3 _Position, Quaternion _Rotation,  float _Scale)
    {
        GameObject go_temp = Instantiate(_SpawnableObject.SpawnGameobject, this.transform.position + _Position, _Rotation) as GameObject;
        go_temp.transform.localScale = go_temp.transform.localScale * _Scale;
        return go_temp;
    }

    float SetScale(CSpawnableObject2D _Object)
    {
        if (_Object.RandomScale)
        {
            return Random.Range(_Object.ScaleMin, _Object.ScaleMax);
        }
        else
            return 1.0f;
    }

    //misc properties Layer Tag ObjectName SpriteRenderer
    void SetOtherProperties(CSpawnableObject2D _Obj, GameObject _GO) //_Obj info and GO already spawned
    {
        if (_Obj.ObjectName != "")
            _GO.name = _Obj.ObjectName;

        if (_Obj.Tag != "")
        {
            _GO.tag = _Obj.Tag;
        }

        if (_Obj.Layer != "")
            _GO.layer = LayerMask.NameToLayer(_Obj.Layer);

        SpriteRenderer tempRenderer = _GO.GetComponent<SpriteRenderer>(); // get component of GO can be empty

        if (tempRenderer != null)
        {
            if (_Obj.SortingLayer != "")
                tempRenderer.sortingLayerName = _Obj.SortingLayer;
            if (_Obj.SLayerOrder != 0)
                tempRenderer.sortingOrder = _Obj.SLayerOrder;
        }
        else
        {
            SpriteRenderer tRenderer = _GO.AddComponent<SpriteRenderer>();
            if (_Obj.SortingLayer != "")
                tRenderer.sortingLayerName = _Obj.SortingLayer;
            if (_Obj.SLayerOrder != 0)
                tRenderer.sortingOrder = _Obj.SLayerOrder;
        }
    }

    //physics
    void ApplyPhysicsToGO(CSpawnableObject2D _Obj, GameObject _GO)
    {
        Vector3 v3_tempRandomDirection = _Obj.PhysicsForceDirection;
        Rigidbody2D tRigid = _GO.GetComponent<Rigidbody2D>(); //get Component because it may not have it! And that is ok.

        if (_Obj.HasRigidbody)
        {
            if (tRigid == null)
            {
                tRigid = _GO.AddComponent<Rigidbody2D>();
            }

            if (_Obj.RandomForceValue)
            {
                _Obj.NotRandomForce = Random.Range(_Obj.ForceValueRndMin, _Obj.ForceValueRndMax); //change stored variable
            }

            if (_Obj.RndForceDirection)
            {
                v3_tempRandomDirection = new Vector3(Random.Range(-360f, 360f), Random.Range(-360f, 360f), 0f).normalized;
            }
            
            tRigid.AddForce(v3_tempRandomDirection * _Obj.NotRandomForce); //those variables are changed above if conditions are true

            if (_Obj.Gravity)
                tRigid.gravityScale = 1f;
            else
                tRigid.gravityScale = 0f;

            tRigid.isKinematic = _Obj.IsKinematic;

        }
    }

    //NO GAME OBJECT ERROR HANDLING:
    void EnableAllDisableAll(bool _Set)
    {
        if(_Set) //nonGameObject so error and we dont spawn anything!
        {
            _OnStart = false; _OnDisable = false; _OnDestroy = false;
        }
    }
    bool CheckForNonGameObjects()
    {
        for (int i = 0; i < Container.Length; i++)
        {
            if (Container[i].SpawnGameobject.GetType() != typeof(GameObject) || Container[i].SpawnGameobject == null)
            {
                Debug.LogError("ERROR 01 : Type not Supported please make sure its a GameObject " + "| Element " + i + " | Type:" + Container[i].SpawnGameobject.GetType() + " | Objectname: " + Container[i].SpawnGameobject.name);
                return true;
            }
        }
        return false;
    }

    #region Definitions

    [System.Serializable]
    public class CSpawnableObject2D
    {
        [Header("#SpawnObject")]
        public GameObject SpawnGameobject = null;

        [Header("#GameobjectSettings")]
        public string ObjectName = "";  //wenn string name leer wird der name nicht geendert clone immer abschneiden
        public string Tag = ""; //If tag empty use untagged
        public string SortingLayer = "";    // 0 == default layer
        public int SLayerOrder = 0;
        public string Layer = "";   // ""


        //Basic
        [Header("#Rotation")]
        public bool RotationRandom = false;     //Random Rotation
        //[HideIfNot("RotationRandom", false, "Container", "RotationDegree")]
        public float RotationDegree = 0f;           //If zero no rotation
        //[HideIfNot("RotationRandom", true, "Container", "RotationMin")]
        public float RotationMin = -180f;       //Rotates from the RotationDegree rotation! not From the start Rotation
       // [HideIfNot("RotationRandom", true, "Container", "RotationMax")]
        public float RotationMax = 180f;            //Rotates from the RotationDegree rotation! not From the start Rotation

        //Random Position
        [Header("#Position")]
        [Tooltip("Wir benutzen immer 0 für Z! Ausser in speziellen Sonderfällen!!!")]
        public bool PreserveParentZPosition = false;
        public bool RandomSpawn = true;         //if true random position relative to RelativeSpawnPos else at RelativeSpawnPos
      //  [HideIfNot("RandomSpawn", false, "Container", "RelativeSpawnPos")]
        [Tooltip("Wir benutzen immer 0 für Z! Ausser in speziellen Sonderfällen!!!")]
        public Vector3 RelativeSpawnPos = Vector3.zero;                         //relative to transform.position of destroyed object
      //  [HideIfNot("RandomSpawn", true, "Container", "RandomXmin")]
        public float RandomXmin = -2.5f;
     //   [HideIfNot("RandomSpawn", true, "Container", "RandomXmax")]
        public float RandomXmax = 2.5f;
     //   [HideIfNot("RandomSpawn", true, "Container", "RandomYmin")]
        public float RandomYmin = -2.5f;
     //   [HideIfNot("RandomSpawn", true, "Container", "RandomYmax")]
        public float RandomYmax = 2.5f;
      //  [HideIfNot("RandomSpawn", true, "Container", "RandomZmin")]
        public float RandomZmin = 0f;
      //  [HideIfNot("RandomSpawn", true, "Container", "RandomZmax")]
        public float RandomZmax = 0f;

        //Random Scale
        [Header("#Scale")]
        public bool RandomScale = false;            //if true random scale else defaul scale
       // [HideIfNot("RandomScale", true, "Container", "ScaleMin")]
        public float ScaleMin = 0.8f;
       // [HideIfNot("RandomScale", true, "Container", "ScaleMax")]
        public float ScaleMax = 1.2f;



        //Physics
        [Header("#Physics")]
        [Tooltip("Deaktiviert die komplette #Physics Kategorie auch wenn sie nicht komplett versteckt ist im inspector!")]
        public bool HasRigidbody = false;  //if true checks if there is a rigidbody and adds one
        

      //  [HideIfNot("HasRigidbody", true, "Container", "Gravity")]
        public bool Gravity = false;

      //  [HideIfNot("HasRigidbody", true, "Container", "IsKinematic")]
        public bool IsKinematic = false;
      //  [HideIfNot("HasRigidbody", true, "Container", "RandomForceValue")]
        public bool RandomForceValue = false;        //if true makes a random force power betwen min and max else ForceValue
      //  [HideIfNot("RandomForceValue", false, "Container", "NotRandomForce")]
        public float NotRandomForce = 2f;
      //  [HideIfNot("RandomForceValue", true, "Container", "ForceValueRndMin")]
        public float ForceValueRndMin = 2f;

      //  [HideIfNot("RandomForceValue", true, "Container", "ForceValueRndMax")]
        public float ForceValueRndMax = 2f;
       // [HideIfNot("HasRigidbody", true, "Container", "RndForceDirection")]
        public bool RndForceDirection = true;        //if true Random Direction 360 degrees

       // [HideIfNot("RndForceDirection", false, "Container", "PhysicsForceDirection")]
        public Vector2 PhysicsForceDirection = Vector2.zero;

    }

    #endregion
}
}
