﻿using UnityEngine;
using System.Collections;

public class GameTime : MonoBehaviour
{

	public static float TIME = 450f;
	public static bool GAME_OVER = false;
	public static bool WIN = false;
	public static readonly float CRITICAL_TIME_REMAING = 30f;
	public static float gametime = 0f;

    private bool over = false;

    public GameObject Cake = null;
    public GameObject Voodooo = null;
    public RecepyChecker Checker = null;

    void Awake ()
	{
		gametime = GameTime.TIME;
        Checker = GameObject.Find("trybtn").GetComponent<RecepyChecker>();
	}

	public static void ResetGameStats ()
	{
		GameTime.TIME = GameTime.gametime;
		GameTime.GAME_OVER = false;
		GameTime.WIN = false;
	}

	void Update ()
	{
        if(GameTime.GAME_OVER && GameTime.WIN && over == false)
        {
            Debug.LogWarning("WIN");
            over = true;
            Instantiate(Cake, Vector3.up * 13f, Quaternion.Euler(270f,0f,0f));
            Checker.Finish();
        }

        if (GameTime.GAME_OVER && !GameTime.WIN && over == false)
        {
            over = true;
            Debug.LogWarning("LOOSE");
            Instantiate(Voodooo, Vector3.up * 13f, Quaternion.Euler(9f, 0f, 0f));
            Checker.Finish();
        }

        if (!GAME_OVER)
			GameTime.TIME -= Time.deltaTime;

		if (TIME <= 0f) {
			Debug.LogWarning ("Timer Zero GAME OVER");
			GameTime.GAME_OVER = true;
			GameTime.WIN = false;
		}
	}
}
