﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayAgain : MonoBehaviour {

	public void Play()
    {
        //reset game time and scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameTime.ResetGameStats();

        //reset game state
        RecepyChecker.RECEPIE = 0;
        RecepyChecker.RECEPIE_SET = 0;
    }
}
