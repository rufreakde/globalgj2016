﻿using UnityEngine;
using System.Collections;

public class TriggerBools : MonoBehaviour {

    public Items LastItem = null;
    public ProceduralMaterial substance;

    public bool somethingInside = false;
    public int countInside = 0;

    [Range(0f,0.65f)]
    public float Glow = 0.45f;
    public float emission = 0f;

    void Start()
    {
        substance.CacheProceduralProperty("emissivee", true);
    }

    void OnTriggerStay(Collider other)
    {
        LastItem = other.GetComponent<Items>();
        if (LastItem)
        {
            somethingInside = true;
        }
    }

    void Update()
    {
        if (countInside <= 0)
        {
            somethingInside = false;
            StartCoroutine(Fade(false));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        LastItem = other.GetComponent<Items>();
        if (LastItem)
        {
            countInside++;
            StopAllCoroutines();
            StartCoroutine(Fade(true));
            //Debug.Log("ENTER set Value of: " + LastItem.set._Value + " to: " + true);
            LastItem.set._Value = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
    LastItem = other.GetComponent<Items>();

        if (LastItem)
        {
            countInside--;
            if (!somethingInside)
            {
                StopAllCoroutines();
                StartCoroutine(Fade(false));
            }
            LastItem.set._Value = false;
        }
    }

    IEnumerator Fade(bool _In)
    {
        if (_In)
        {
            while (emission <= Glow)
            {
                //Debug.Log("FADE in: " + emission);
                emission += Time.deltaTime;
                SetPBREmission(emission, substance);
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            while (emission >= 0f)
            {
                //Debug.Log("FADE out: " + emission);
                emission -= Time.deltaTime;
                SetPBREmission(emission, substance);
                yield return new WaitForEndOfFrame();
            }
        }

        //TODO DISABLE SOUND
    }

    void SetPBREmission(float _Value, ProceduralMaterial _Mat)
    {
        //if (_Mat.HasProceduralProperty("_emissivee"))
        //{
        _Mat.SetProceduralFloat("emissivee", emission);
        _Mat.RebuildTextures();
        //}
    }
}
