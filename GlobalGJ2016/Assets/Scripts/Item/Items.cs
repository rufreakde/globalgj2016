﻿using UnityEngine;
using System.Collections;

public class Items : MonoBehaviour {

    public Set set = new Set();
    public DataBase db = null;

	// Use this for initialization
	void OnEnable ()
    {
        if (db == null)
            Debug.LogError("DATABASE MISSING ON: " + this.ToString());

        this.gameObject.tag = "Item";
        
        set._Value = SetCorespondingValue(set._Item);
	}

    public bool SetCorespondingValue(Item _Type)
    {
        for( int i=0; i< db.List[RecepyChecker.RECEPIE].Set.Length; i++)
        {
            for (int j=0; j < db.List[RecepyChecker.RECEPIE].Set[i]._State.Length; j++)
            {
                if (db.List[RecepyChecker.RECEPIE].Set[i]._State[j]._Item == set._Item)
                {

                    //Debug.Log("Init of item: " + this.gameObject.name + "   Value: " + db.List[RecepyChecker.RECEPIE].Set[i]._State[j]._Value);
                    return db.List[RecepyChecker.RECEPIE].Set[i]._State[j]._Value;
                }
            }
        }

        //Debug.LogError("Error item: " + set._Item + " not found in coresponding list at: " + RecepyChecker.RECEPIE);
        return false;
    }
}
