﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Items))]
public class ObjectMover : MonoBehaviour
{
	private static readonly float PICKUP_INCREASE = 0.5f;
	private static readonly float FIXED_DISTANCE = 3.7f;
	private Rigidbody rigidBody;
	private Plane movePlane;
	private float hitDist;
	private float time;
	private Ray cameraRay;
	private Vector3 point;
	private Vector3 corPoint;
	private Vector3 targetPosition;
	private Items ValueInside = null;
	private bool Inside = false;
	private ParticleSystem particleSystem;
	public float particleStartLifeTime = 0.8f;
	public float particleStartSpeed = 0.3f;
	public float particleStartSize = 0.18f;
	public float particleGravityModifier = -1f;

	// Use this for initialization
	void Awake ()
	{
		rigidBody = GetComponent<Rigidbody> ();

		ValueInside = GetComponent<Items> ();

		targetPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);

		setupParticleSystem ();
	}

	private void setupParticleSystem ()
	{
		GameObject particleObject = new GameObject ();
		particleObject.AddComponent<ParticleSystem> ();

		particleSystem = particleObject.GetComponent<ParticleSystem> ();

		particleObject.transform.position = transform.position;
		particleObject.transform.parent = this.transform;

		enableParticleEffect (false);

		particleSystem.startLifetime = particleStartLifeTime;
		particleSystem.startSpeed = particleStartSpeed;
		particleSystem.startSize = particleStartSize;
		particleSystem.gravityModifier = particleGravityModifier;
	}

	private void setParticleColor ()
	{
		int red = Random.Range (1, -1);
		int blue = Random.Range (1, -1);
		int green = Random.Range (1, -1);
		particleSystem.startColor = new Color (red, green, blue, 1);
	}

	private void enableParticleEffect (bool isEnabled)
	{
		particleSystem.enableEmission = isEnabled;
	}

	private void checkCollisionAndSnap ()
	{
		rigidBody.useGravity = true;

		if (!Inside) {
			transform.position = targetPosition;
		} 
	}

    public void ResetPosition()
    {
        transform.position = targetPosition;
    }

	void OnMouseDown ()
	{
		pickUpItem ();
		movePlane = new Plane (-Camera.main.transform.forward, transform.position); // find a parallel plane to the camera based on obj start pos;
	}

	private void pickUpItem ()
	{
		float newYPosition = transform.position.y + PICKUP_INCREASE;
		transform.position = new Vector3 (transform.position.x, newYPosition, transform.position.z);
		rigidBody.useGravity = false;

		setParticleColor ();
		enableParticleEffect (true);
	}

	void OnMouseUp ()
	{
		enableParticleEffect (false);
		checkCollisionAndSnap ();
	}

	bool CheckIfInside ()
	{
		return ValueInside.set._Value;
	}

	void OnMouseDrag ()
	{
		Inside = CheckIfInside ();
		moveItem ();
	}

	private void moveItem ()
	{
		cameraRay = Camera.main.ScreenPointToRay (Input.mousePosition); // shoot a ray at the obj from mouse screen point

		if (movePlane.Raycast (cameraRay, out hitDist)) { // finde the collision on movePlane
			point = cameraRay.GetPoint (hitDist); // define the point on movePlane
			time = -(FIXED_DISTANCE - cameraRay.origin.y) / (cameraRay.origin.y - point.y); // the x,y or z plane you want to be fixed to
			corPoint.x = cameraRay.origin.x + (point.x - cameraRay.origin.x) * time; // calculate the new point t futher along the ray
			corPoint.y = cameraRay.origin.y + (point.y - cameraRay.origin.y) * time;
			corPoint.z = cameraRay.origin.z + (point.z - cameraRay.origin.z) * time;
			transform.position = corPoint;
		}
	}
}
