﻿using UnityEngine;
using System.Collections.Generic;

public class SetPrefabs : MonoBehaviour
{
    private RecepyChecker recepieScript;

    public GameObject ItemsParent;

    public List<GameObject> Items;
    public List<GameObject> SpawnPlaces;


    // Use this for initialization
    void OnEnable()
    {
        setUpNewSet();
    }

    public void setUpNewSet()
    {
        recepieScript = GameObject.Find("trybtn").GetComponent<RecepyChecker>();

        System.Random rnd = new System.Random();
        //int rndID = rnd.Next(0, recepieScript.Recepies.List.Count);
        int rndID = rnd.Next(4, 6);


        RecepyChecker.RECEPIE = rndID; 

        Debug.LogWarning(rndID);

        Recepie r = recepieScript.Recepies.List[rndID];
        State spawnState = r.Set[0];
        
        List<Set> staticItems = new List<Set>();
        List<Set> spawnItems = new List<Set>(); 


        for (var i = 0; i <= spawnState._State.Length - 1; i++)
        {
            Set item = spawnState._State[i];
            if (item._Value)
            {
                if (!item._Static)
                {
                    spawnItems.Add(item);
                }
            }

            if (item._Static)
            {
                staticItems.Add(item);
            }
        }

        // Generate the spawn list
        List<int> spawnList = generateSpawnList(spawnItems);

        // Spawn the dynamic items
        spawnTheItems(spawnItems, spawnList);

        // Spawn the static items
        SetStaticState(staticItems);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetKeyDown("space"))
    //        setUpNewSet();
    //    //    spawnItems(null);
    //}

    /// <summary>
    /// Destroy the remaining items
    /// </summary>
    void resetTable()
    {
        var children = new List<GameObject>();
        foreach (Transform child in ItemsParent.transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
        foreach (GameObject item in items)
        {
            //TO DO Phil what is this??!?!
            //if(item.GetComponent<Items>().set._Static == false)
            //if (item.name != "hips")
                item.SetActive(false);
        }

        GameObject[] itemss = GameObject.FindGameObjectsWithTag("ItemStatic");
        foreach (GameObject item in itemss)
        {
            //TO DO Phil what is this??!?!
            //if(item.GetComponent<Items>().set._Static == false)

            if (item.name != "VoodooMan" && item.name != "VoodooWoman")
                item.SetActive(false);
            else
            {
                //sonderbehandlung
                item.transform.parent.parent.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Spawn the objects from the generated SpawnList
    /// </summary>
    void spawnTheItems(List<Set> spawnItems, List<int> spawnList)
    {
        resetTable();
        int n = 0;

        foreach (Set i in spawnItems)
        {
            GameObject cSpawnPoint = SpawnPlaces[spawnList[n]];
            GameObject myitem = getItemByName(i._Item.ToString());
            if (myitem == null)
            {
                Debug.Log("An dynamic object was not found: '" + i._Item.ToString() + "' Check the item names");
            }
            else
            {
                GameObject newItem = Instantiate(myitem, cSpawnPoint.transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0)) as GameObject;
                newItem.SetActive(true);
                newItem.transform.parent = ItemsParent.transform;
            }
            n++;
        }

    }

    /// <summary>
    /// Spawn the objects from the generated SpawnList
    /// </summary>
    void SetStaticState(List<Set> spawnStaticItems)
    {
        foreach (Set i in spawnStaticItems)
        {
            
            GameObject myitem = getItemByName(i._Item.ToString());

            //Debug.Log("Name: " + i._Item.ToString() +"obj"+ myitem.name + " value: " + i._Value);

            if (myitem == null)
            {
                Debug.Log("An static object was not found. Check the item names " + i._Item.ToString());
            }
            else
            {
                //TODO
                if (myitem.name != "VoodooMan" && myitem.name != "VoodooWoman")
                {
                    Debug.Log("Item: " + i + "  " + i._Item.ToString());
                    getItemByName(i._Item.ToString()).transform.FindChildByTagRecursive("ItemStatic").gameObject.SetActive(i._Value);
                }
                else
                    getItemByName(i._Item.ToString()).SetActive(i._Value);

            }
        }
    }

    private GameObject getItemByName(string name)
    {
        foreach (GameObject g in Items)
        {
            if (g.name == name)
            {
                return g;
            }
        }
        return null;
    }

    /// <summary>
    /// Generate a random spawnlist
    /// </summary>
    /// <param name="items"></param>
    /// <returns></returns>
    private List<int> generateSpawnList(List<Set> items)
    {
        System.Random rnd = new System.Random();
        List<int> spawnHere = new List<int>();

        // Generate a spawn order
        for (int i = 0; i <= items.Count - 1; i++)
        {
            // Random SpawnNumber
            int place = rnd.Next(1, SpawnPlaces.Count - 1);
            int error = 0;
            while (spawnHere.Contains(place) && error <= 500)
            {
                error++;
                place = rnd.Next(1, SpawnPlaces.Count - 1);
            }

            spawnHere.Add(place);
        }


        return spawnHere;
    }

    /// <summary>
    /// Get objects by name
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private GameObject getObjectByName(string name)
    {
        return (GameObject)this.GetType().GetField(name).GetValue(this);
    }



}
