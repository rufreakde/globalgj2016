﻿using UnityEngine;
using System.Collections;

public class DebugShow : MonoBehaviour {

    public bool DEBUG = false;

	void OnGUI()
    {
        if (DEBUG)
        {
            GUI.color = Color.red;

            GUILayout.Label(" Game Over " + GameTime.GAME_OVER);
            GUILayout.Label(" Game WIN " + GameTime.WIN);
            GUILayout.Label(" TIME " + GameTime.TIME);
            GUILayout.Label("  ");
            GUILayout.Label(" RECEPIE " + RecepyChecker.RECEPIE);
            GUILayout.Label(" RECEPIE STATE " + RecepyChecker.RECEPIE_SET);
        }
    }
}
