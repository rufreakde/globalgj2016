﻿using UnityEngine;
using System.Collections;
using Dev6;

public class RecepyChecker : MonoBehaviour
{
    public ScreenShake CameraShakeScript = null;
    [Range(0.1f, 4f)]
    public float ShakeTime = 1f;
    [Range(0.1f, 10f)]
    public float ShakeStrengh = 4f;

    public static int RECEPIE = 0;
    public static int RECEPIE_SET = 0;

    public bool DEBUG = false;

    public DataBase Recepies = null;

    public float WrongPenalty = 30f;
    public GameObject PlayAgainButton = null;

    public ActualState ActualStateScript = null;

    void Start()
    {
        if (Recepies == null || Recepies.List.Count <= 0)
        {
            if (DEBUG)
                Debug.LogError("The Database of " + this.ToString() + " is not filled! please fill it with data");
        }
    }

    public void Finish()
    {
        if(GameTime.GAME_OVER == true)
        {
            if(GameTime.WIN == true)
            {
                PlayAgainButton.SetActive(true);
                this.gameObject.SetActive(false);
            }
            else
            {
                PlayAgainButton.SetActive(true);
                this.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// just makes apenalty of "WrongPenalty" to the time
    /// </summary>
    public void CheckCurrentSet()
    {
        if (Recepies.List[RECEPIE].Set.Length <= RecepyChecker.RECEPIE_SET + 1)
        {
            if (DEBUG)
                Debug.Log("GAME OVER - null reference prevention! Deactivating the button! ARRAY LENGTH:" + Recepies.List[RECEPIE].Set.Length + "  COUNTER OF STATIC  " + (RecepyChecker.RECEPIE_SET + 1));
            PlayAgainButton.SetActive(true);
            this.gameObject.SetActive(false);
            GameTime.WIN = true;
            GameTime.GAME_OVER = true;
            return;
        }

        if (Check())
        {
            RecepyChecker.RECEPIE_SET++;
            if (OverShot()) // check if overshot then game over == true and win!
            {
                if (DEBUG)
                    Debug.LogWarning("WIN GAME OVER");
                GameTime.WIN = true;
                GameTime.GAME_OVER = true;
            }
        }
        else
        {
            GameTime.TIME -= WrongPenalty;

            //screnshake!
            CameraShakeScript.Shake(ShakeTime, ShakeStrengh);

            if (OverShot()) // check if overshot then game over == false and win!
            {
                if (DEBUG)
                    Debug.LogWarning("LOSS GAME OVER");
                GameTime.WIN = true;
                GameTime.GAME_OVER = true;

                //TODO GAME OVER LOSS
            }
        }
    }

    /// <summary>
    /// checks if the state variable is bigger than the state lenght!
    /// </summary>
    /// <returns></returns>
    private bool OverShot()
    {
        if (DEBUG)
            Debug.Log("R: " + RecepyChecker.RECEPIE + "   SET: " + RecepyChecker.RECEPIE_SET);

        //get State:
        State tRecepieNeeded = Recepies.List[RecepyChecker.RECEPIE].Set[RecepyChecker.RECEPIE_SET];

        if (tRecepieNeeded._State.Length <= RecepyChecker.RECEPIE_SET)
        {
            if (DEBUG)
                Debug.Log("LAST FINISHED!");
            return true;
        }
        if (DEBUG)
            Debug.Log("NEXT");
        return false;
    }

    /// <summary>
    /// Checks the current state with the +1 state and if wrong returns false else true if right
    /// </summary>
    /// <returns></returns>
    private bool Check()
    {
        //get State:
        State tRecepieNeeded = Recepies.List[RecepyChecker.RECEPIE].Set[RecepyChecker.RECEPIE_SET + 1]; // +1 because we check the next state!

        for (int i = 0; i < tRecepieNeeded._State.Length; i++)
        {
            if (ActualStateScript.ACTUAL_STATE._State.Length <= i)
            {
                Debug.LogError("Error you have less states in current state than you have in the actual state! numbers: " + i + "  " + tRecepieNeeded._State[i]._Item + "   ITEM:  " + tRecepieNeeded._State[i]._Value);
                return false;
            }

            if (tRecepieNeeded._State[i]._Value != ActualStateScript.ACTUAL_STATE._State[i]._Value)
            {
                Debug.Log("NO MATCH " + tRecepieNeeded._State[i]._Item + " != " + ActualStateScript.ACTUAL_STATE._State[i]._Item + "  ITEM:  " + tRecepieNeeded._State[i]._Value + " != " + ActualStateScript.ACTUAL_STATE._State[i]._Value);

                if (tRecepieNeeded._State[i]._Value != ActualStateScript.ACTUAL_STATE._State[i]._Value)
                {
                    Debug.LogWarning("CHECK ORDER!   numbers: " + tRecepieNeeded._State[i]._Item + " != " + ActualStateScript.ACTUAL_STATE._State[i]._Item + "  ITEM:  " + tRecepieNeeded._State[i]._Value + " != " + ActualStateScript.ACTUAL_STATE._State[i]._Value);
                }
                return false;
            }
        }

        if (ActualStateScript.ACTUAL_STATE._State.Length != tRecepieNeeded._State.Length)
        {
            Debug.LogError("THE CURRENT STATE HAS A DIFFERENT SET AS THE NEXT STATE:   current: " + ActualStateScript.ACTUAL_STATE._State.Length + "  next: " + tRecepieNeeded._State.Length + "   numbers for next: " + RecepyChecker.RECEPIE + " " + (RecepyChecker.RECEPIE_SET + 1));
        }
        else
        {
            //TODO play sound on righ success
            Debug.Log("SUCCESS MATCH");
        }

        return true;
    }

}
