﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActualState : MonoBehaviour
{
    public bool DEBUG = false;
    public sState ACTUAL_STATE = new sState();
    public DataBase db;

    public List<Items> itemlist = null;

    //void Awake()
    //{
    //    ACTUAL_STATE = new sState();
    //}

    void OnEnable()
    {
        InitFirstState();
        CheckItems(true);
    }

    void Update()
    {
            CheckItems(false);
    }

    void CheckItems(bool reset)
    {
        GameObject[] tgos = GameObject.FindGameObjectsWithTag("Item");
        itemlist.Clear();

        for (int i = 0; i < tgos.Length; i++)
        {
            itemlist.Add(tgos[i].GetComponent<Items>());
            //Debug.Log("i: " + i + " -- " + tgos[i].GetComponent<Items>().);
        }

        for (int j = 0; j < ACTUAL_STATE._State.Length; j++)
        {
            for (int i = 0; i < itemlist.Count; i++)
            {
                //Debug.Log("Max A: " + ActualState.ACTUAL_STATE._State.Length + " Actual A: " + j + "    Max Set: " + itemlist.Count + "  Actual Set: " + i);

                if (reset && ACTUAL_STATE._State[j]._Static == false)
                {
                    itemlist[i].set._Value = false;
                    ACTUAL_STATE._State[j]._Value = false;
                }

                if (itemlist[i] == null)
                {
                    Debug.LogError("Size" + itemlist.Count + " - Nr " + i);
                }

                else if (ACTUAL_STATE._State[j]._Item == itemlist[i].set._Item && ACTUAL_STATE._State[j]._Static == false)
                {
                    ACTUAL_STATE._State[j]._Value = itemlist[i].set._Value;
                }
            }
        }
    }

    public void InitFirstState()
    {
        if (db == null)
        {
            Debug.LogError("Error Database missing: " + this.ToString());
        }

        //Debug.Log("States: " + RecepyChecker.RECEPIE + "  " + RecepyChecker.RECEPIE_SET);
        ACTUAL_STATE._State = SetStructShit();  //(sSet[])db.List[RecepyChecker.RECEPIE].Set[RecepyChecker.RECEPIE_SET]._State;
    }

    public sSet[] SetStructShit()
    {
        //db.List[RecepyChecker.RECEPIE].Set[RecepyChecker.RECEPIE_SET];
        Set[] tSet = db.List[RecepyChecker.RECEPIE].Set[RecepyChecker.RECEPIE_SET]._State;
        sSet[] tStruct = new sSet[tSet.Length];

        for (int i = 0; i < tSet.Length; i++)
        {
            tStruct[i]._Item = tSet[i]._Item;
            tStruct[i]._Value = tSet[i]._Value;
            tStruct[i]._Static = tSet[i]._Static;
        }

        return tStruct;
    }

    void OnGUI()
    {
        if (DEBUG)
        {
            for (int i = 0; i < ACTUAL_STATE._State.Length; i++)
            {
                GUILayout.Label("State: " + ACTUAL_STATE._State[i]._Item + " |Bool: " + ACTUAL_STATE._State[i]._Value);
            }
        }
    }

    [System.Serializable]
    public struct sState
    {
        public sSet[] _State; 
    }

    [System.Serializable]
    public struct sSet
    {
        public Item _Item;
        public bool _Value;
        public bool _Static;
    }
}
