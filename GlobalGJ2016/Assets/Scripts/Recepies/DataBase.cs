﻿/*******************
* Rudolf Chrispens *
*******************/

#region using
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#endregion

[System.Serializable]
public class DataBase : ScriptableObject
{
    public List<Recepie> List = new List<Recepie>();
}

[System.Serializable]
public class Recepie
{
    public State[] Set = new State[0];
}

[System.Serializable]
public class State
{
    public Set[] _State = new Set[0]; 
}

[System.Serializable]
public class Set
{
    public Item _Item = Item.Book;
    /// <summary>
    /// true inside or on false outside of circle or on!
    /// </summary>
    public bool _Value = false;
    public bool _Static = true;
}

public enum Item
{
    Gold_Pocket,
    Ring,
    Schuessel,
    VoodooMan,
    VoodooWoman,
    Bottle1,
    BloodSchuessel,
    Book,
    Bone,
    Totem,
    Salt,
    Gem,
    Bottle2,  
    Candle,
    Candle_Small,
    Bottle3,
    Moerser,
}

