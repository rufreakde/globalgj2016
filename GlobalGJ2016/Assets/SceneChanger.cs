﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

	public void ChangeScene(string _Scene)
    {
        SceneManager.LoadScene(_Scene);
    }
}
