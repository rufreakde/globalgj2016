﻿using UnityEngine;
using System.Collections;

public class SoundOnOff : MonoBehaviour {

    public static bool SoundSettings = true;
    public GameObject Other;

	public void ToggleSound(bool _Set)
    {
        SoundSettings = _Set;
        this.gameObject.SetActive(false);
        Other.SetActive(true);
    }
}
