﻿using UnityEngine;
using System.Collections;

public class HandAnimation : MonoBehaviour {

    private Animator MyAnimator = null;

	// Use this for initialization
	void Start ()
    {
        MyAnimator = this.GetComponent<Animator>();
	}
	
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            MyAnimator.SetBool("Grabbing", true);
        }

        if(Input.GetMouseButtonUp(0))
        {
            MyAnimator.SetBool("Grabbing", false);
        }
    }

}
