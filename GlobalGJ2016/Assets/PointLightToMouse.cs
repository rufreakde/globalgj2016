﻿using UnityEngine;
using System.Collections;

public class PointLightToMouse : MonoBehaviour {

    public Transform PointLight = null;

    void Awake()
    {
        PointLight = this.transform;
        //Cursor.visible = false;
    }

    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 400f))
        {
            PointLight.position = hit.point;
        }
    }
}
