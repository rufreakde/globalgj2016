﻿using UnityEngine;
using System.Collections;

public class GOResetTrigger : MonoBehaviour {

    public Transform ResetPosition = null;

    private ObjectMover Movescript;

	void OnTriggerEnter(Collider _Other)
    {
        Movescript = _Other.GetComponent<ObjectMover>();

        if (Movescript)
        {
            ResetVelocity(_Other.gameObject);
            Movescript.ResetPosition();
        }
        else
        {
            if (ResetPosition)
                ResetVelocity(_Other.gameObject);
                _Other.transform.position = ResetPosition.position;
        }
    }

    void ResetVelocity(GameObject _GO)
    {
        Rigidbody tRB = _GO.GetComponent<Rigidbody>();
        tRB.velocity = Vector3.zero;
    }
}
