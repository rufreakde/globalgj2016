﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowTimer : MonoBehaviour
{
	private Text timerText;
	private float gameTimeRemaining;

    private Text RoundText;
    private int Round;

	// Use this for initialization
	void Start ()
	{
		timerText = GameObject.Find ("ShowTimerText").GetComponent<Text> ();
        RoundText = GameObject.Find("Round").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update ()
	{
		gameTimeRemaining = GameTime.TIME <= 0f ? 0f : GameTime.TIME;
        Round = RecepyChecker.RECEPIE_SET;
		setTextColorRunningOutOfTime ();
		timerText.text = gameTimeRemaining.ToString ("F0");
        RoundText.text = Round.ToString();
    }
	
	private void setTextColorRunningOutOfTime ()
	{
		if (gameTimeRemaining < GameTime.CRITICAL_TIME_REMAING)
        {
			timerText.color = new Color (0.87f, 0.33f, 0.37f);
		} else
        {
			timerText.color = new Color (1f, 1f, 1f, 1f);
		}
	}
}
